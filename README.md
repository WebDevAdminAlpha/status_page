# Status Page

Implementation spike for https://gitlab.com/gitlab-org/gitlab/issues/199876

## Development

### Setup

```
bin/setup
rails server
open http://127.0.0.1:3000
```
